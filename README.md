![francis_2015_figure1_ctz_to_fyy.png](https://bitbucket.org/repo/ezq99p/images/3356726632-francis_2015_figure1_ctz_to_fyy.png)
# Data for published paper #
Francis, W.R., N.C. Shaner, L.M. Christianson, M.L. Powers, and S.H.D. Haddock (2015) [Occurrence of Isopenicillin-N-Synthase homologs in bioluminescent ctenophores and implications for coelenterazine biosynthesis.](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0128742) **PLOS ONE** 10:6

or [Pubmed link](https://www.ncbi.nlm.nih.gov/pubmed/26125183)

## Ctenophore data on NCBI ##
All FYY proteins and non-photoproteins can be found in [src](https://bitbucket.org/wrf/fyy-nonphotoprotein/src), as nucleotides, proteins, and alignments. RAxML trees with bipartitions are also available.

Data were uploaded to NCBI, with [Popset for IPNS-like FYY proteins](http://www.ncbi.nlm.nih.gov/popset/702084477) and [Popset for photoproteins](http://www.ncbi.nlm.nih.gov/popset/702084463)

## Transcriptome data for Hormiphora californensis ##
[Hormiphora californensis](http://www.ncbi.nlm.nih.gov/biosample/SAMN03539694) Trinity [assembly here](https://bitbucket.org/wrf/ctenophore-transcriptomes/src), normalized, reads at [SRR1992642](http://www.ncbi.nlm.nih.gov/sra/SRX1006956)